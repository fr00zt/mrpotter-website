---
layout: page
title: Word Dashboard
permalink: /words.html
---

Draft novel dashboard. Like a NaNoWriMo that never ends. (🔗[Click here for more information
](/writing/2020/02/20/six-novel.html).)

__Current Goal:__ Average 800 words/day measured over two weeks

__Current Reset Point:__ 3/9/21

### Progress

__Current average: <span id="average">Pending</span>__

<svg id="graph" width="700" height="500"></svg>

<!-- D3.js setup -->
<script src="https://d3js.org/d3.v5.min.js"></script>
<style>
    .bar {
        fill: #000;
    }
    .todayBar {
      fill: #070;
    }
    .newwords {
      fill: #242;
      z-index: 1000;
    }
    .barovertarget {
        fill: #0f0;
    }
    .barundertarget {
      fill: #99c;
      opacity: .5;
    }
    .futuretarget {
      fill: #ccc;
      display: none;
    }
    .todaytarget {
      fill: #fa0;
    }
    .averagetarget {
      fill: #009;
    }
    .hidden {
      display: none;
    }
    .tooltip {
      position: absolute;
      z-index: 10;
      visibility: hidden;
      background-color: black;
      text-align: center;
      padding: 4px;
      font-weight: bold;
      color: #0f0;
    }
</style>

<script>
    var svg = d3.select("#graph"),
        margin = 40,
        width = svg.attr("width") - margin,
        height = svg.attr("height") - margin;

    var durt = function(dert) {
      // Rubbish function to negate local timezones, and trick Date() into being EST
      // Hack thrown all about willy nilly like duct tape
      // What this logic really needs is a more pure year/month/day representation, since nothing here gives two figs about hours, minutes, or seconds.

      var _userOffset = new Date(dert).getTimezoneOffset()*60*1000, _hour = 60*60*1000;
      var _easternOffset = 4*_hour;

      if (dert instanceof Date)
        dert = dert.getTime();

      var d00t = new Date(Math.floor((dert - _userOffset + _easternOffset) / (_hour*24)) * _hour*24);
      return d00t;
    }
    
    //d3.csv("http://localhost:4000/assets/wc.csv").then(function(rdata) {
    // Moved to another domain for live updates without a silly GitHub redeploy
    d3.csv("https://akgames.com/cgi-bin/wordshow.cgi?d=" + Math.random(10000)).then(function(rawdata) {     
    // What follows is an incredibly convoluted average words-per-day computation.
    // Know that this kludge was written while barely registering as awake.
    // Eventually I'll rewrite it with moment.js, but right now, my word count is the priority

    // Pass #1: Clean up data
    var rdata = [];
    for (x = 0; x < rawdata.length; x++) {
      if (rawdata[x].wordcount) {
        rdata.push({date: durt(new Date(rawdata[x].date)), wordcount: parseInt(rawdata[x].wordcount)});
      }
    }

    var today = durt((new Date()).setHours(12));

    var twoWeeksAgo = durt(new Date((new Date(today)).setDate(today.getDate()-13)));
    //var twoWeeksAgo = new Date("2021-03-09");
    while(twoWeeksAgo < rdata[1].date && twoWeeksAgo.getTime() < today.getTime()) {
      twoWeeksAgo = durt(new Date((new Date(twoWeeksAgo)).setDate(twoWeeksAgo.getDate()+1)));
    }

    endday = today;
    var days = 1, words = 0, x = 0;
    var currentDay = new Date(twoWeeksAgo);
    var startingWords = 0;
    var targetAverage = 400;
    
    var data = [];

    // Pass #2: get the average
    while(currentDay.getTime() <= endday.getTime()) {
      while(rdata[x].date.getTime() < currentDay.getTime())
        if (x < rdata.length - 1) {
          x++;
        } else
          break;
      if (rdata[x].date.getTime() == currentDay.getTime()) {
        words += rdata[x].wordcount - rdata[x-1].wordcount;
      }
      days++;
      currentDay = durt(new Date(currentDay.setDate(currentDay.getDate() + 1)));
    }
    days = days - 1; // Remove the extra addition and where tomorrow is also calculated
    targetAverage = Math.floor((words)/(days))
    document.getElementById("average").textContent=targetAverage + " words/day (over " + (days) + " days)";

    currentDay = durt(new Date(twoWeeksAgo));
    x = 0;
    days = 1;
    words = 0;

    // Pass #3: Fill in the graph data
    while(currentDay.getTime() <= endday.getTime()) {
      // Data needs to move forward to current date being rendered
      while(rdata[x].date.getTime() < currentDay.getTime())
        if (x < rdata.length - 1) {
          if (rdata[x].date < twoWeeksAgo || startingWords == 0)
            startingWords = rdata[x+1].wordcount;
          x++;
        } else
          break;
      if (rdata[x].date.getTime() == currentDay.getTime()) {
        // Data point to include
        words += rdata[x].wordcount - rdata[x-1].wordcount;//rdata[x].wordcount;
        data.push({
          date: durt(rdata[x].date),
          wordcount: rdata[x].wordcount,
          newwords: rdata[x].wordcount - rdata[x-1].wordcount,
          //targetwords: rdata[x].wordcount + targetAverage
          targetwords: startingWords + targetAverage*(days-1)
        });
      } else if (currentDay.getTime() >= endday.getTime()) {
        if ( rdata[x].wordcount == null)
          console.log("0 " + JSON.stringify(rdata[x]));
        // Last day, post the target to hit
        data.push({
          date: durt(new Date(endday)),
          wordcount: rdata[x].wordcount,
          newwords: 0,
          //targetwords: rdata[x].wordcount + targetAverage
          targetwords: startingWords + targetAverage*(days-1)
        });
      } else if (rdata[x].date > currentDay && rdata[x].date > twoWeeksAgo) {
        if ( rdata[x-1].wordcount == null)
          console.log("1 " + JSON.stringify(rdata[x-1]));
        // Missed day, populate target
        data.push({
          date: durt(new Date(currentDay)),
          wordcount: rdata[x-1].wordcount,
          newwords: 0,
          //targetwords: rdata[x-1].wordcount + targetAverage
          targetwords: startingWords + targetAverage*(days-1)
        });
      } else if (x == rdata.length-1) {
        // Missed day, populate target (no gap closer)
        if ( rdata[x].wordcount == null)
          console.log("2 " + JSON.stringify(rdata[x]));
        data.push({
          date: durt(new Date(currentDay)),
          wordcount: rdata[x].wordcount,
          newwords: 0,
          //targetwords: rdata[x].wordcount + targetAverage
          targetwords: startingWords + targetAverage*(days-1)
        });
      }
      days++;
      currentDay = durt(new Date(currentDay.setDate(currentDay.getDate() + 1)));
      console.log("Processing "+ currentDay);
    }
    data.push({
          date: durt(new Date(currentDay)),
          wordcount: rdata[rdata.length-1].wordcount,
          targetwords: startingWords + targetAverage*days
        });
    days = days - 1; // Remove the extra addition and where tomorrow is also calculated
    document.getElementById("average").textContent=Math.floor((words)/(days)) + " words/day (over " + (days) + " days)";
    
    var min = durt(new Date(d3.min(data.map(function(d) { return d.date }))));
    var max = durt(new Date(d3.max(data.map(function(d) { return d.date }))));
    data.unshift({date: durt(min.setDate(min.getDate() - 1)), wordcount: startingWords});
    data.push({date: durt(max.setDate(max.getDate() + 2)), wordcount: startingWords});

    var xScale = d3.scaleTime()
          .domain(d3.extent(data, d => d.date))
          .range([20, width-80])
        yScale = d3.scaleLinear().range([height, 0]);

    let sourceNames = [],
        sourceCount = [];
    var tooltip = d3.select("body")
      .append("div")
      .attr('class', 'tooltip');

    var g = svg.append("g").attr("transform", "translate(" + 80 + "," + 20 + ")");
      
    yScale.domain([startingWords - 1000, d3.max(data, function(d) {
        return d.wordcount;
    }) + 2000]);

    var barWidth = ((width-margin*2) / (xScale.ticks(d3.timeDay).length +1))/2;

    var addFixedGoal = function(wordsToAdd, goalLabel, alwaysShow) {
      bars.append('rect')
      .attr("class", function(d) {
        var wordsplus = (d.wordcount - d.newwords) + wordsToAdd;
        if (!d.targetwords || d.date > today)
          return "hidden"
        else
        if (d.wordcount >= wordsplus)
          return "barovertarget";
        else if (d.date.toString() != today.toString() && alwaysShow)
          return "futuretarget";
        else if (alwaysShow)
          return "todaytarget";
        else
          return "hidden";
      })
      .attr("x", function(d) { return xScale(d.date) - barWidth + margin; })
      .attr("y", function(d) { return yScale((d.wordcount - d.newwords) + wordsToAdd); })
      .attr("width", barWidth*2)
      .attr("height", function(d) {
        var wordsplus = (d.wordcount - d.newwords) + wordsToAdd;
        if (d.wordcount >= wordsplus)
          return 16;
        else
          return 8;
      })

      bars.append("text")
      .attr("class", function(d) {
          var wordsplus = (d.wordcount - d.newwords)+wordsToAdd;
          if (d.wordcount >= wordsplus)
            return "";
          else
            return "hidden";
        })
      .text(goalLabel)
      .attr("x", function(d) { return xScale(d.date) + margin; })
      .attr("y", function(d) {
          return yScale((d.wordcount - d.newwords) + wordsToAdd) + 14;
      })
      .attr("font-family", "Arial, Helvetica, sans-serif")
      .attr("font-size", "12px")
      .attr("font-weight", 900)
      .attr("fill", "#050")
      .attr("text-anchor", "middle");
    }

    g.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(xScale).ticks(d3.timeDay).tickFormat(d3.timeFormat("%m/%d")));

    g.append("g")
      .call(d3.axisLeft(yScale).tickFormat(function(d) {
          return d + " words";
      }).ticks(10))
      .append("text")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text("wordcount");
  
    // "Today" Indicator
    g.append("rect")
        .attr("x", xScale(today) - barWidth + margin)
        .attr("y", 0)
        .attr("width", barWidth*2)
        .attr("height", yScale(data[data.length - 2].wordcount))
        .attr("fill", "#cc8")
        .attr("opacity", 0.2);

    g.append("text")
          .text("Today")
          .attr("x", xScale(today) + margin)
          .attr("y", 20)
          .attr("font-family", "sans-serif")
          .attr("font-size", "10px")
          .attr("fill", "#666")
          .attr("text-anchor", "middle");

    bars = g.selectAll(".bar").data(data).enter();
    
    bars.append("rect")
      .attr("class", function(d) {
        if (d.date < today)
          return "newwords";
        else if (d.date == today.toString()) // Bit wonky that this was needed.
          return "todayBar";
        else
          return "hidden";
      })
      .attr("x", function(d) {
          return xScale(d.date) - barWidth + margin;
      })
      .attr("y", function(d) {
          return yScale(d.wordcount);
      })
      .attr("width", barWidth*2)
      .attr("height", function(d) {
        console.log("Trying " + JSON.stringify(d))
          return height - yScale(d.wordcount);
      })
      .on("mouseover", function(d) {
        return tooltip.style("visibility", "visible").text(d.newwords + " new words");
      })
      .on("mousemove", function() {
        return tooltip.style("top", (event.pageY - 30) + "px")
          .style("left", event.pageX + "px");
      });

      // New Words
      bars.append("rect")
      .attr("class", "bar")
      .attr("x", function(d) {
          return xScale(d.date) - barWidth + margin;
      })
      .attr("y", function(d) {
          return yScale(d.wordcount - d.newwords);
      })
      .attr("width", barWidth*2)
      .attr("height", function(d) {
        console.log("Trying " + JSON.stringify(d))
          return height - yScale(d.wordcount - d.newwords);
      })
      .on("mouseover", function(d) {
        return tooltip.style("visibility", "visible").text(d.wordcount + " words");
      })
      .on("mousemove", function() {
        return tooltip.style("top", (event.pageY - 15) + "px")
          .style("left", event.pageX + "px");
      });

      // Target bars
      bars.append('rect')
      .attr("class", function(d) {
        if (!d.targetwords)
          return "hidden"
        else
        if (d.wordcount >= d.targetwords)
          return "averagetarget";
        else if (d.wordcount < d.targetwords && d.date <= today)
          //return "futuretarget";
          return "barundertarget";
        else
          return "barundertarget";
          //return "futuretarget";
      })
      .attr("x", function(d) {
          return xScale(d.date) - barWidth + margin;
      })
      .attr("y", function(d) {
          return yScale(d.targetwords);
      })
      .attr("width", barWidth*2)
      .attr("height", function(d) {
          return 2;
      })
      .on("mouseover", function(d) {
        if (d.targetwords)
          return tooltip.style("visibility", "visible").text("Target: " + d.targetwords);
      })
      .on("mousemove", function(d) {
        if (d.targetwords)
        return tooltip.style("top", (event.pageY - 30) + "px")
          .style("left", event.pageX + "px");
      });


      addFixedGoal(800, "800", true);
      addFixedGoal(1666, "1.6K", false);
      addFixedGoal(2400, "2.4K", false);

    }).catch(function(error) {
        console.log(error);
    });
</script>

---
layout: page
title: About
permalink: /about/
---

![resist television](/assets/resist.jpg)

I create things. 

Then I put them here. 

Some say I am magical. 

Others, a full walking catastrophe.

